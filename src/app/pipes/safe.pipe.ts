import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'safe'
})
export class SafePipe implements PipeTransform {
  /**Le safe prend l'url en base  64 pour la déclarer comme sans danger (safe*/
  /** The safe takes the url in base 64 to declare it as safe */
  constructor(private domsatinizer:DomSanitizer) {}
  transform(url): any {
    return this.domsatinizer.bypassSecurityTrustResourceUrl(url);
  }

}
