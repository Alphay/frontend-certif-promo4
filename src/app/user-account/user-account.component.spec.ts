import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAccountComponent } from './user-account.component';
import { DebugElement } from "@angular/core";
import { By } from "@angular/platform-browser";

xdescribe('UserAccountComponent', () => {
  let component: UserAccountComponent;
  let fixture: ComponentFixture<UserAccountComponent>;
  let submitEl: DebugElement;
  let loginEl: DebugElement;
  let passwordEl: DebugElement;

  beforeEach(async(() => {

    // refine the test module by declaring the test component
    TestBed.configureTestingModule({
      declarations: [ UserAccountComponent ]
    })
    .compileComponents();
    
      // create component and test fixture
      fixture = TestBed.createComponent(UserAccountComponent);

      // get test component from the fixture
      component = fixture.componentInstance;

      submitEl = fixture.debugElement.query(By.css('button'));
      loginEl = fixture.debugElement.query(By.css('input[type=email]'));
      passwordEl = fixture.debugElement.query(By.css('input[type=password]'));
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
