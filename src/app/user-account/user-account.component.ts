import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { EComStoreApiService } from '../services/e-com-store-api.service';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.scss']
})
export class UserAccountComponent implements OnInit {
  
/**Logic to identify as a user using the services */
  constructor(private service:EComStoreApiService, private router : Router,private userService:UserService) { }
  email:string;
  password:string;
  ngOnInit() {
  }
  check(event){
    console.log(event);
    this.email = event.target.value
  }
  check2(event){
    this.password = event.target.value
                                                                                                                            
    console.log(event);
  }
  submit() {
    console.log("email:",this.email,"password:",this.password);
    
    this.service.login({password:this.password,email:this.email}).subscribe(value =>{
      console.log(value);
      this.service.setToken(value['token'])
      console.log(value);
      this.service.getById(value['userId']).subscribe(value => {
        console.log(value);
      this.router.navigate([''])
        this.service.setLogged(true)
        this.userService.setUser(value['user'])
      });
    })
  }
}
