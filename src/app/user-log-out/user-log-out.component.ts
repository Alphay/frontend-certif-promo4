
import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { EComStoreApiService } from '../services/e-com-store-api.service';

@Component({
  selector: 'app-user-log-out',
  templateUrl: './user-log-out.component.html',
  styleUrls: ['./user-log-out.component.scss']
})
export class UserLogOutComponent implements OnInit {

  constructor(private service:EComStoreApiService, private router : Router,private userService:UserService) { }

  ngOnInit() {
    this.userService.setUser({});
  }
}

