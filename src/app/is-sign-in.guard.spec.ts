import { TestBed, async, inject } from '@angular/core/testing';

import { IsSignInGuard } from './is-sign-in.guard';

describe('IsSignInGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [IsSignInGuard]
    });
  });

  it('should ...', inject([IsSignInGuard], (guard: IsSignInGuard) => {
    expect(guard).toBeTruthy();
  }));
});
