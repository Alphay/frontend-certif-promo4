import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { EComStoreApiService } from '../services/e-com-store-api.service';

@Component({
  selector: 'app-ad-page',
  templateUrl: './ad-page.component.html',
  styleUrls: ['./ad-page.component.scss']
})
export class AdPageComponent implements OnInit {
  urls = ['../../assets/icon-picture.svg'];
  produits = [];

  constructor(private service:EComStoreApiService, private router : Router) { }
  name: string;
  email:string;
  phoneNumber: number;
  description:string;
  title:string;
  category:string;
  price:number;
  newImage:string;
à

  ngOnInit() {
  }


  checkName(event){
    console.log(event);
    this.name = event.target.value
  }

  checkEmail(event){
    console.log(event);
    this.email = event.target.value
  }
  checkPhoneNumber(event){
    console.log(event);    
    this.phoneNumber = event.target.value.toString();
  }

  checkTitle(event){
    console.log(event);
    this.title = event.target.value                                                                                                                         
  }
  checkCategory(event){
    console.log(event);
    this.category = event.target.value
  }
  checkPrice(event){
    console.log(event);
    this.price = parseInt(event.target.value)
  }

  checkDescription(event){
    console.log(event);
    this.description = event.target.value                                                                                                                         
  }
/**Connexion d'un nouveau utilisateur */

newProduct() {
  let product = {name:this.title,price:this.price,category:this.category,email:this.email,phoneNumber:this.phoneNumber,description:this.description,userName:this.name,images:this.newImage}
  console.log("TEST ",product);
  
  this.service.addProduct(product).subscribe(value=>{
    console.log("value",value);
    

  })
}

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
        var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
            var reader = new FileReader();

            reader.onload = (event) => {
              this.newImage = event.target["result"];
              console.log(event.target["result"]);
                this.urls.push(event.target["result"]); 
            }

            reader.readAsDataURL(event.target.files[i]);
        }
    }
  }

}
