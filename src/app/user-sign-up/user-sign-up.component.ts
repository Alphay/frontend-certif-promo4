import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EComStoreApiService } from '../services/e-com-store-api.service';

@Component({
  selector: 'app-user-sign-up',
  templateUrl: './user-sign-up.component.html',
  styleUrls: ['./user-sign-up.component.scss']
})
export class UserSignUpComponent implements OnInit {

  constructor(private service:EComStoreApiService, private router : Router) { }
  name: string;
  userName:string;
  email:string;
  password:string;
  confirmPassword: string;

  ngOnInit() {
  }

  checkName(event){
    console.log(event);
    this.name = event.target.value
  }

  checkEmail(event){
    console.log(event);
    this.email = event.target.value
  }
  checkPassWord(event){
    console.log(event);    
    this.password = event.target.value
  }
  checkConfPassword(event){
    console.log(event);
    this.confirmPassword = event.target.value                                                                                                                         
  }
  checkUserName(event){
    console.log(event);
    this.userName = event.target.value                                                                                                                         
  }
  submit() {
    let newUser = {name:this.name,password:this.confirmPassword, email:this.email, userName:this.userName}
    console.log("new user",newUser);
    
    this.service.newUser(newUser).subscribe(value =>{
      console.log(value);
      this.router.navigate(["user",value])
      //this.service.user = value;
      console.log(value);
    })
  }

}
