import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { User } from '../entities/user';
import { Observable, BehaviorSubject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class EComStoreApiService {
  apiDomain: string = "http://137.74.113.253:3000"
  token: string;
  responseLoginData: string;
  panier: Object;
  user:User;
  isLogged:boolean=false;
  isloggedValidator:BehaviorSubject<Boolean> = new BehaviorSubject(false);
  constructor(private http: HttpClient) { }

  private getToken() {
    return this.token;
  }
/**(Set token) send a token to gain access */
  setHttpOptions() {
    let head = new HttpHeaders({ 'Content-Type': 'application/json',
    'Authorization': this.getToken()})
   let httpOptions = {
      headers: head
    };
    return httpOptions;
  }
  setToken(token) {
    console.log(token);
    
  localStorage.setItem('nom du token',JSON.stringify(token));
    this.token = token;
  }

/**New user login */
  newUser(user) {
    return this.http.post<User>(`${this.apiDomain}/auth/register`, user)
  }
  login(user) {
    console.log("user:",user);
    
    return this.http.post(`${this.apiDomain}/auth/login`, user);
  }

  setLogged(value:boolean) {
    this.isLogged = value
  }
  getLogged() {
    return this.isLogged
  }
/**add a product and save it */
  addProduct(product) {
    return this.http.post(`${this.apiDomain}/product/register`, product)
    
  }
/**(Get user byId): Recover a user by his Id **/
  getById(id){
    return this.http.get(`${this.apiDomain}/users/${id}`,this.setHttpOptions());
  }
}


