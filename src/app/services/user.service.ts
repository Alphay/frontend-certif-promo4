import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { headersToString } from 'selenium-webdriver/http';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  //user:Object;
  user:BehaviorSubject<Object> = new BehaviorSubject({})
  constructor(private httpClient: HttpClient) { }

  getUser() {  
    return this.user.getValue()
  }
  setUser(user){
    //this.user = user;
    this.user.next(user)
    console.log("current user",this.user);
  }
}
