import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Produit } from '../entities/produit';
import { Observable, of } from 'rxjs';
import { map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {
  produits
  apiDomain: string = "http://137.74.113.253:3000";
  constructor(private httpClient: HttpClient) { }
  /** Method to recover a product by the id */   
  getAllProduit() {
    return this.httpClient.get(`${this.apiDomain}/products`).pipe(
      tap(value => {
        console.log(value);
        this.produits = value;
      })
    )
  }
  /** Method to recover a product by the id */
  getProduit(id) {
    return this.httpClient.get<Produit>(`${this.apiDomain}/products/${id}`).pipe(
      tap(value => {
        console.log(value);
        
        // this.localStorageService.setItem(`produit.${id}`, value);     
      })
    )
  }

  /**Method to recover all products */   
  getProduits() {
    return this.produits;
    console.log("get current produits", this.produits);
  }
  /** Method to recover a product by the id */
  setUser(produits){
    this.produits = produits;
    console.log("current produits",this.produits);  
  }
}