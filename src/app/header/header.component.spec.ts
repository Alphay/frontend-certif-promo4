import { async, ComponentFixture, TestBed,  } from '@angular/core/testing';
import { HeaderComponent } from './header.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderComponent ],imports: [HttpClientModule, RouterTestingModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  // test the existence of the image element svg of the header component.html
  it('should see a logo svg', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector("#header-logo-mobile").src).toContain("http://localhost:9876/assets/logo_A-B-A.svg")
  });
  // test the boolean is valid from the  header component.ts
  it('should  isValid is Boolean', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(component.isValid).toBe(false)
  });
});
