import { Component, OnInit } from '@angular/core';
import { Produit } from 'src/app/entities/produit';
import { PanierService } from 'src/app/services/panier.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { EComStoreApiService } from '../services/e-com-store-api.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  
  panier : Produit [];
  user:Object = {};
  isValid:Boolean = false;
  constructor(private panierService: PanierService, private UserService:UserService,private router:Router,private api:EComStoreApiService) { 
    this.UserService.user.subscribe(user=>{
      this.user = user
      console.log(user);
      /**when does not have your interface on creates an object with a hack ["name"] */
      if(user["name"]) this.isValid = true;

      
    })
  }

  ngOnInit() {

    this.panier = this.panierService.getAllProduits();
    
  }

  /**method logout of application */
  logOut(){
    this.user = null;
    this.UserService.setUser({})
    this.isValid = false;
    this.api.setToken("");
    this.api.setLogged(false);
    this.router.navigate(['/login']);
  }
}
