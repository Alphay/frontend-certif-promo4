import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProduitListComponent } from './produit-list/produit-list.component';
import { ProduitFicheComponent } from './produit-fiche/produit-fiche.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PanierComponent } from 'src/app/panier/panier.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { UserSignUpComponent } from './user-sign-up/user-sign-up.component';
import { AdPageComponent } from './ad-page/ad-page.component';
import { UserLogOutComponent } from './user-log-out/user-log-out.component';
import { IsSignInGuard } from './is-sign-in.guard';




const routes: Routes = [
  { path: '', component: ProduitListComponent },
  { path: 'user/:id', component: ProduitListComponent },
  { path: 'produit-fiche/:id', component: ProduitFicheComponent },
  { path: 'panier', component: PanierComponent }, 
  { path: 'user-account', component: UserAccountComponent },
  { path: 'user-sign-up', component: UserSignUpComponent },
  { path: 'user-log-out', component: UserLogOutComponent },
  { path: 'ad-page', component: AdPageComponent, canActivate: [IsSignInGuard] },



  // { path: 'movie', component:  MovieComponent},
  { path: '**', component: NotFoundComponent }, 
]

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})
export class AppRoutingModule {}
