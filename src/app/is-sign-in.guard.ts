import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { EComStoreApiService } from './services/e-com-store-api.service';

@Injectable({
  providedIn: 'root'
})
export class IsSignInGuard implements CanActivate {
  constructor(private userService:EComStoreApiService,private router:Router) {

  }
  canActivate() {
    const isLogged = this.userService.getLogged()
    if (!isLogged) {
      this.router.navigate([""])
    }
    return isLogged;
  }
}
